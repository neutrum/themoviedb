//
//  Responses.h
//  TheMovieDB
//
//  Created by E.D.Neutrum on 19/01/17.
//  Copyright © 2017 usertech. All rights reserved.
//

#ifndef Responses_h
#define Responses_h
    #import "UTBaseResponse.h"
    #import "UTPopularMoviesResponse.h"
    #import "UTMovieDetailRef.h"
#endif /* Responses_h */
