//
//  TheMovieDBClient.m
//  TheMovieDB
//
//  Created by E.D.Neutrum on 23/01/17.
//  Copyright © 2017 usertech. All rights reserved.
//

#import "TheMovieDBClient.h"

@implementation TheMovieDBClient

+ (id)shared {
    static TheMovieDBClient *sharedTMDbClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedTMDbClient = [[self alloc] init];
    });
    return sharedTMDbClient;
}

- (id)init {
    if (self = [super init]) {
        
    }
    return self;
}

- (void)getPopularMovies:(UTPopularMoviesRequest *)popularMovieRequest  withCompletion:(void (^)(UTPopularMoviesResponse *response))callbackBlock {
    
    MostPopularEndpoint *mpe = [MostPopularEndpoint endpointWithParameters:[popularMovieRequest toDictionary]];
    [mpe getWithCompletion:^(UTPopularMoviesResponse *response, NSError *error) {
        callbackBlock(response);
    }];
    
}
@end
