//
//  UTCreateUpdateHelper.h
//  TheMovieDB
//
//  Created by E.D.Neutrum on 24/01/17.
//  Copyright © 2017 usertech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm.h>
#import <UIKit/UIKit.h>
#import "UTPosterModel.h"
#import "UTPopularMoviesResponse.h"

@interface UTCreateUpdateHelper : NSObject
+(void)createOrUpdateMoviePage:(UTPopularMoviesResponse *)moviePage;
@end
