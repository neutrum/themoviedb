//
//  UTCreateUpdateHelper.m
//  TheMovieDB
//
//  Created by E.D.Neutrum on 24/01/17.
//  Copyright © 2017 usertech. All rights reserved.
//

#import "UTCreateUpdateHelper.h"

@implementation UTCreateUpdateHelper

+(void)createOrUpdateMoviePage:(UTPopularMoviesResponse *)moviePage {
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [UTPopularMoviesResponse createOrUpdateInDefaultRealmWithValue:moviePage];
    [realm commitWriteTransaction];
}

+(void)createOrUpdatePosterImage:(UIImage *)poster {
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [UTPosterModel createOrUpdateInDefaultRealmWithValue:@{@"posterImage": UIImagePNGRepresentation(poster)}];
    [realm commitWriteTransaction];
}

@end
