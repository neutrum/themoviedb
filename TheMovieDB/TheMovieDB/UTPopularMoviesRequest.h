//
//  UTPopularMoviesRequest.h
//  TheMovieDB
//
//  Created by E.D.Neutrum on 23/01/17.
//  Copyright © 2017 usertech. All rights reserved.
//

#import "UTBaseRequest.h"

@interface UTPopularMoviesRequest : UTBaseRequest
    @property NSInteger page;
    @property (nonatomic , strong) NSString<Optional> *language;
    @property (nonatomic , strong) NSString<Optional> *region;
@end
