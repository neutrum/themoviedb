//
//  TheMovieDBClient.h
//  TheMovieDB
//
//  Created by E.D.Neutrum on 23/01/17.
//  Copyright © 2017 usertech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TheMovieDBAPI.h"

@interface TheMovieDBClient : NSObject
+ (id)shared;
- (void)getPopularMovies:(UTPopularMoviesRequest *)popularMovieRequest  withCompletion:(void (^)(UTPopularMoviesResponse *response))callbackBlock;
@end
