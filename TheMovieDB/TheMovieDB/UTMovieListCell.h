//
//  UTMovieListCell.h
//  TheMovieDB
//
//  Created by E.D.Neutrum on 23/01/17.
//  Copyright © 2017 usertech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UTMovieListCell : UITableViewCell
    @property (weak, nonatomic) IBOutlet UILabel *movieRelease;
    @property (weak, nonatomic) IBOutlet UIImageView *moviePoster;
    @property (weak, nonatomic) IBOutlet UITextView *movieTitle;
@end
