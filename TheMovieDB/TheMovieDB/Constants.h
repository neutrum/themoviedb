//
//  Constants.h
//  TheMovieDB
//
//  Created by E.D.Neutrum on 23/01/17.
//  Copyright © 2017 usertech. All rights reserved.
//

#ifndef Constants_h
#define Constants_h
    #define LANGUAGE @"en_US"
    #define IMG_URL @"https://image.tmdb.org/t/p/w300"
#endif /* Constants_h */
