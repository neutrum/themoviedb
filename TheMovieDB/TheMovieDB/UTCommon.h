//
//  UTCommon.h
//  TheMovieDB
//
//  Created by E.D.Neutrum on 23/01/17.
//  Copyright © 2017 usertech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Reachability.h"

@interface UTCommon : NSObject
+(UIColor *)colorFromHexString:(NSString *)hexString;
+(BOOL)isDeviceConnected;
@end
