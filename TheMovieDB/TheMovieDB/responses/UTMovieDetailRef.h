//
//  UTMovieDetailRef.h
//  TheMovieDB
//
//  Created by E.D.Neutrum on 19/01/17.
//  Copyright © 2017 usertech. All rights reserved.
//

#import "UTBaseResponse.h"

@protocol UTMovieDetailRef @end
@interface UTMovieDetailRef : UTBaseResponse
    @property (nonatomic , strong) NSString *posterPath;
    @property BOOL adult;
    @property (nonatomic , strong) NSString *overview;
    @property (nonatomic , strong) NSString *releaseDate;
    @property (nonatomic , strong) NSString *originalTitle;
    @property (nonatomic , strong) NSString *originalLanguage;
    @property (nonatomic , strong) NSString *title;
    @property (nonatomic , strong) NSString *backdropPath;
    @property (nonatomic , strong) NSNumber<RLMFloat> *popularity;
    @property (nonatomic , strong) NSNumber<RLMFloat> *voteAverage;
    @property NSInteger voteCount;
    @property NSInteger id;
    @property BOOL video;
    @property (nonatomic, retain) NSData *posterImage;
    @property (nonatomic, retain) NSData *backdropImage;
@end
