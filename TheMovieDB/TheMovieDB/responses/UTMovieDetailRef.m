//
//  UTMovieDetailRef.m
//  TheMovieDB
//
//  Created by E.D.Neutrum on 19/01/17.
//  Copyright © 2017 usertech. All rights reserved.
//

#import "UTMovieDetailRef.h"

@implementation UTMovieDetailRef

+ (NSDictionary *)mapping {
    NSMutableDictionary *mapping = [NSMutableDictionary dictionary];
    mapping[@"id"] = @"id";
    mapping[@"posterPath"] = @"poster_path";
    mapping[@"adult"]    = @"adult";
    mapping[@"overview"]    = @"overview";
    mapping[@"releaseDate"] = @"release_date";
    mapping[@"generIds"] = @"gener_ids";
    mapping[@"originalTitle"] = @"original_title";
    mapping[@"originalLanguage"] = @"original_language";
    mapping[@"title"] = @"title";
    mapping[@"backdropPath"] = @"backdrop_path";
    mapping[@"popularity"] = @"popularity";
    mapping[@"voteAverage"] = @"vote_average";
    mapping[@"voteCount"] = @"vote_count";
    mapping[@"video"] = @"video";
    return mapping;
}

@end
RLM_ARRAY_TYPE(UTMovieDetailRef)
