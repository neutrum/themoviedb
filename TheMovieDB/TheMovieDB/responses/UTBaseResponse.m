//
//  UTBaseResponse.m
//  TheMovieDB
//
//  Created by E.D.Neutrum on 19/01/17.
//  Copyright © 2017 usertech. All rights reserved.
//

#import "UTBaseResponse.h"

@implementation UTBaseResponse

+ (NSDictionary *)mapping {
    NSMutableDictionary *mapping = [NSMutableDictionary dictionary];
    // Note keypaths in associated JSON
    mapping[@"statusMessage"] = @"status_message";
    mapping[@"statusCode"]    = @"status_code";
    return mapping;
}

@end
