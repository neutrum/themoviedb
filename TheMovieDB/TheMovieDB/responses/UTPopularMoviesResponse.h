//
//  UTPopularMoviesResponse.h
//  TheMovieDB
//
//  Created by E.D.Neutrum on 19/01/17.
//  Copyright © 2017 usertech. All rights reserved.
//

#import "UTBaseResponse.h"
#import "UTMovieDetailRef.h"

@interface UTPopularMoviesResponse : UTBaseResponse
    @property NSInteger id;
    @property NSInteger page;
    @property NSInteger totalPages;
    @property NSInteger totalResults;
    @property (strong, nonatomic) RLMArray<UTMovieDetailRef>  *results;
@end
