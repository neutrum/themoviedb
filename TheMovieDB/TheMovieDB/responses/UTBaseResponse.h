//
//  UTBaseResponse.h
//  TheMovieDB
//
//  Created by E.D.Neutrum on 19/01/17.
//  Copyright © 2017 usertech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Genome/Genome.h>
#import <Realm/Realm.h>

@interface UTBaseResponse : RLMObject<GenomeObject>
    @property (nonatomic , strong) NSString *statusMessage;
    @property NSInteger statusCode;
@end
