//
//  UTPopularMoviesResponse.m
//  TheMovieDB
//
//  Created by E.D.Neutrum on 19/01/17.
//  Copyright © 2017 usertech. All rights reserved.
//
//@property NSInteger page;
//@property NSInteger totalPages;
//@property NSInteger totalResults;
//@property (strong, nonatomic) NSArray<UTMovieDetailRef *>  *results;

#import "UTPopularMoviesResponse.h"

@implementation UTPopularMoviesResponse

+ (NSDictionary *)mapping {
    NSMutableDictionary *mapping = [NSMutableDictionary dictionary];
    mapping[@"page"] = @"page";
    mapping[@"totalPages"]    = @"total_pages";
    mapping[@"totalResults"]    = @"total_results";
    mapping[@"results@UTMovieDetailRef"] = @"results";
    return mapping;
}

+(NSString *)primaryKey {
    return @"id";
}

@end

RLM_ARRAY_TYPE(UTPopularMoviesResponse)
