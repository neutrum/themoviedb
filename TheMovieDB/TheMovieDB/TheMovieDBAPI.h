//
//  TheMovieDBAPI.h
//  TheMovieDB
//
//  Created by E.D.Neutrum on 19/01/17.
//  Copyright © 2017 usertech. All rights reserved.
//

#define BASE_URL @"https://api.themoviedb.org/3/"


#import <Foundation/Foundation.h>
#import <Polymer/PLYEndpoint.h>
#import "AFNetworking.h"
#import "Responses.h"
#import "Requests.h"
#import "MBProgressHUD.h"

@interface TheMovieDBBaseEndpoint : PLYEndpoint
@end

@interface MostPopularEndpoint: TheMovieDBBaseEndpoint
@end



