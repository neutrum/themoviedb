//
//  UTCommon.m
//  TheMovieDB
//
//  Created by E.D.Neutrum on 23/01/17.
//  Copyright © 2017 usertech. All rights reserved.
//

#import "UTCommon.h"

@implementation UTCommon

+(UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass "#"
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

+(BOOL)isDeviceConnected {
    Reachability *network = [Reachability reachabilityWithHostname:@"google.com"];
    NetworkStatus status = [network currentReachabilityStatus];
    
    BOOL statusResult = YES;
    
    switch (status) {
        case NotReachable:
            statusResult = NO;
            break;
        default:
            break;
    }
    
    return statusResult;
}

@end
