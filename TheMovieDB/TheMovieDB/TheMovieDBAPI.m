//
//  TheMovieDBAPI.m
//  TheMovieDB
//
//  Created by E.D.Neutrum on 19/01/17.
//  Copyright © 2017 usertech. All rights reserved.
//

#import "TheMovieDBAPI.h"

@implementation TheMovieDBBaseEndpoint

- (NSString *)baseUrl {
    return BASE_URL;
}

@end

@implementation MostPopularEndpoint

- (Class)returnClass {
    return [UTPopularMoviesResponse class];
}

- (NSString *)endpointUrl {
    return @"movie/popular";
}
@end
