//
//  UTMovieListViewController.m
//  TheMovieDB
//
//  Created by E.D.Neutrum on 23/01/17.
//  Copyright © 2017 usertech. All rights reserved.
//
#import "UTMovieListViewController.h"

@interface UTMovieListViewController () {
    NSMutableArray *movies;
    UTPopularMoviesRequest *request;
}

@end

@implementation UTMovieListViewController
@synthesize searchController;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialSetup];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

-(void)initialSetup {
    
    // set navigationbar title
    self.navigationItem.title = @"Movie List";
    
    // allocate datasource for tableview
    movies = [NSMutableArray new];
    
    // setup request object
    request = [UTPopularMoviesRequest new];
    request.page = 1;
    request.language = LANGUAGE;
    
    // remove trailing empty cells
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    // init searchbar
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.delegate = self;
    
    self.definesPresentationContext = YES;
    
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    // pull data
    [self pullMovieData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)pullMovieData {
    
    
    // ONLINE mode
    if ([UTCommon isDeviceConnected]) {
        
        // show loading popup
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        // fetch data from server
        [[TheMovieDBClient shared] getPopularMovies:request withCompletion:^(UTPopularMoviesResponse *response) {
            
            // add response to tableview datasource
            [movies addObjectsFromArray:(NSArray *)response.results];
            
            // create or update movie list page for offline browsing
            [UTCreateUpdateHelper createOrUpdateMoviePage:response];
            
            // reload tableview with new datasource
            [self.tableView reloadData];
            
            // increase page for next pull
            request.page++;
            
            // hide loading popup
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
        }];
    } else {
    // OFFLINE mode
        
        // fetch data for page from offline storage
        RLMResults<UTPopularMoviesResponse *> *moviePage = [UTPopularMoviesResponse objectsWhere:@"page == %ld", (long)request.page];
        
        // add response to tableview datasource
        [movies addObjectsFromArray:(NSArray *)moviePage.firstObject.results];
        
        // reload tableview with new datasource
        [self.tableView reloadData];
        
        // increase page for next pull
        request.page++;
        
    }
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [movies count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UTMovieDetailRef *movieDetail = [movies objectAtIndex:indexPath.row];
    
    UTMovieListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MovieListCell" forIndexPath:indexPath];
    
    cell.moviePoster.layer.cornerRadius  = 35.f;
    cell.moviePoster.layer.masksToBounds = YES;
    
    cell.movieTitle.text = movieDetail.originalTitle;
    cell.movieRelease.text = movieDetail.releaseDate;
    
    [self pullPosterImage:movieDetail forCell:cell];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100.f;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UTMovieDetailRef *movieData = [movies objectAtIndex:indexPath.row];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UTMovieDetailViewController *movieDetailController = [mainStoryboard instantiateViewControllerWithIdentifier:@"MovieDetail"];
    movieDetailController.movieData = movieData;
    
    [self.navigationController pushViewController:movieDetailController animated:YES];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    float endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height;
    if (endScrolling >= scrollView.contentSize.height)
    {
        [self pullMovieData];
    }
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    movies = [NSMutableArray new];
    NSString *searchString = self.searchController.searchBar.text;
    [self searchForText:searchString];
    [self.tableView reloadData];
}

-(void)searchForText:(NSString*)searchText {
    RLMResults<UTMovieDetailRef *> *movieDetails = [UTMovieDetailRef objectsWhere:@"originalTitle CONTAINS %@", searchText];
    NSArray *searchResults = (NSArray *)movieDetails;
    [movies addObjectsFromArray:searchResults];
}

-(void)pullPosterImage:(UTMovieDetailRef *)movieDetail forCell:(UTMovieListCell *)cell {
    RLMRealm *realm = [RLMRealm defaultRealm];
    NSString *imageUrl = [NSString stringWithFormat:@"%@%@", IMG_URL, movieDetail.posterPath];
    
    if ([UTCommon isDeviceConnected]) {
        // asynchronously load poster
        [cell.moviePoster sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"default"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            // store image for offline purpose
            [realm transactionWithBlock:^{
                movieDetail.posterImage = UIImagePNGRepresentation(image);
            }];
        }];
    } else {
        if (movieDetail.posterImage) cell.moviePoster.image = [UIImage imageWithData:movieDetail.posterImage];
    }
}



@end
