//
//  UTMovieListViewController.h
//  TheMovieDB
//
//  Created by E.D.Neutrum on 23/01/17.
//  Copyright © 2017 usertech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UTMovieListCell.h"
#import "TheMovieDBClient.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Constants.h"
#import "UTMovieDetailViewController.h"
#import "UTCommon.h"
#import "UTPosterModel.h"
#import "UTCreateUpdateHelper.h"

@interface UTMovieListViewController : UITableViewController<UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating>
    @property (strong, nonatomic) UISearchController *searchController;
@end
