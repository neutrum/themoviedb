//
//  UTMovieDetailViewController.m
//  TheMovieDB
//
//  Created by E.D.Neutrum on 23/01/17.
//  Copyright © 2017 usertech. All rights reserved.
//

#import "UTMovieDetailViewController.h"

@interface UTMovieDetailViewController ()

@end



@implementation UTMovieDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (nil != _movieData) {
        
        self.navigationItem.title = _movieData.originalTitle;
        
        NSString *imageUrl = [NSString stringWithFormat:@"%@%@", IMG_URL, _movieData.backdropPath];
        
        [_moviePoster sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                            placeholderImage:[UIImage imageNamed:@"default"]];
        
        _movieRelease.text = _movieData.releaseDate;
        
        NSString *rating = [NSString stringWithFormat:@"%@ (%ldx)", _movieData.voteAverage, (long)_movieData.voteCount];
        _movieRating.text = rating;
        _movieDescription.text = _movieData.overview;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)pullPosterImage:(UTMovieDetailRef *)movieDetail {
    RLMRealm *realm = [RLMRealm defaultRealm];
    NSString *imageUrl = [NSString stringWithFormat:@"%@%@", IMG_URL, movieDetail.backdropPath];
    
    if ([UTCommon isDeviceConnected]) {
        // asynchronously load poster
        [_moviePoster sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"default"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            // store image for offline purpose
            [realm transactionWithBlock:^{
                movieDetail.backdropImage = UIImagePNGRepresentation(image);
            }];
        }];
    } else {
        if (movieDetail.backdropImage) _moviePoster.image = [UIImage imageWithData:movieDetail.backdropImage];
    }
}

@end
