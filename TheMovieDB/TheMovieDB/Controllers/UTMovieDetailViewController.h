//
//  UTMovieDetailViewController.h
//  TheMovieDB
//
//  Created by E.D.Neutrum on 23/01/17.
//  Copyright © 2017 usertech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Responses.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Constants.h"
#import "UTCommon.h"

@interface UTMovieDetailViewController : UIViewController
    @property (weak, nonatomic) IBOutlet UIImageView *moviePoster;
    @property (weak, nonatomic) IBOutlet UILabel *movieRelease;
    @property (weak, nonatomic) IBOutlet UILabel *movieRating;
    @property (weak, nonatomic) IBOutlet UITextView *movieDescription;
    @property (weak, nonatomic) UTMovieDetailRef *movieData;
@end
