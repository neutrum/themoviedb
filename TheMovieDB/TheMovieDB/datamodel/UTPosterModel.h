//
//  UTPosterModel.h
//  TheMovieDB
//
//  Created by E.D.Neutrum on 24/01/17.
//  Copyright © 2017 usertech. All rights reserved.
//

#import <Realm/Realm.h>

@interface UTPosterModel : RLMObject
    @property NSInteger id;
    @property (nonatomic, retain) NSData *posterImage;
    @property (nonatomic, retain) NSData *backdropImage;
@end
