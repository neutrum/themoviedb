//
//  UTPosterModel.m
//  TheMovieDB
//
//  Created by E.D.Neutrum on 24/01/17.
//  Copyright © 2017 usertech. All rights reserved.
//

#import "UTPosterModel.h"

@implementation UTPosterModel

+(NSString *)primaryKey {
    return @"id";
}

@end

RLM_ARRAY_TYPE(UTPosterModel)
