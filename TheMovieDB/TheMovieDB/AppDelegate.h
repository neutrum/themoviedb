//
//  AppDelegate.h
//  TheMovieDB
//
//  Created by E.D.Neutrum on 19/01/17.
//  Copyright © 2017 usertech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UTCommon.h"
#import "AFNetworkActivityLogger.h"


#define LANGUAGE @"en_US"
#define IMG_URL @"https://image.tmdb.org/t/p/w300"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

