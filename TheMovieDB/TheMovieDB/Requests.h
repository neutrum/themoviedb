//
//  Requests.h
//  TheMovieDB
//
//  Created by E.D.Neutrum on 19/01/17.
//  Copyright © 2017 usertech. All rights reserved.
//

#ifndef Requests_h
#define Requests_h
    #import "UTBaseRequest.h"
    #import "UTPopularMoviesRequest.h"
#endif /* Requests_h */
