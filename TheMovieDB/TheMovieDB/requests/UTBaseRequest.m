//
//  UTBaseRequest.m
//  TheMovieDB
//
//  Created by E.D.Neutrum on 19/01/17.
//  Copyright © 2017 usertech. All rights reserved.
//
#define API_KEY  @"4aa883f95999ec813b8bfaf319f3972b"

#import "UTBaseRequest.h"

@implementation UTBaseRequest

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
                                                                  @"apiKey" : @"api_key"
                                                                  }];
}


-(NSString *)apiKey {
    return API_KEY;
}

@end
