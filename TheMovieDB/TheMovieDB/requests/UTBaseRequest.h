//
//  UTBaseRequest.h
//  TheMovieDB
//
//  Created by E.D.Neutrum on 19/01/17.
//  Copyright © 2017 usertech. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface UTBaseRequest : JSONModel
@property (nonatomic, strong) NSString *apiKey;
@end
